//
//  SecondViewController.m
//  2048-Game
//
//  Created by Chiragkishore Khubchandani on 2/1/17.
//  Copyright © 2017 Chirag Khubchandani. All rights reserved.
//

#import "SecondViewController.h"


@interface SecondViewController ()


@end

@implementation SecondViewController
@synthesize labels;
@synthesize scoreLabel,animateLabel,Gameover;




- (IBAction)Restart:(id)sender forEvent:(UIEvent *)event {
    //Set all tiles to empty.
    [Gameover setText:@""];
    [scoreLabel setText:@"0"];
    [animateLabel setText:@""];
    
    for(int i=1;i<17;i++){
        [[labels objectAtIndex:i] setText:@""];
    }
    
    
    
    
    
    //Initial 2 tiles declaration
    
    NSInteger Random = arc4random_uniform(16) +1 ;
    
    while(![[[labels objectAtIndex:Random] text] isEqual:@""]){
        Random = arc4random_uniform(16) +1 ;
        
    }
    
    [[labels objectAtIndex:Random] setText:@"2"];
    
    
    NSInteger Random2 = arc4random_uniform(16) +1;
    
    while(![[[labels objectAtIndex:Random2] text] isEqual:@""]){
        Random2 = arc4random_uniform(16) +1 ;
        
    }
    
    [[labels objectAtIndex:Random2] setText:@"2"];
    [self GeneratingColor];
}

    



- (void)viewDidLoad {
    

    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [Gameover setText:@""];
    [animateLabel setText:@""];
    [scoreLabel setText:@"0"];
    
    //swiping gestures declaration
    self.leftSwipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipes:)];
    self.rightSwipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipes:)];
    self.upSwipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipes:)];
    self.downSwipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipes:)];
    
    
    self.leftSwipeGestureRecognizer.direction = UISwipeGestureRecognizerDirectionLeft;
    self.rightSwipeGestureRecognizer.direction = UISwipeGestureRecognizerDirectionRight;
    self.upSwipeGestureRecognizer.direction = UISwipeGestureRecognizerDirectionUp;
    self.downSwipeGestureRecognizer.direction = UISwipeGestureRecognizerDirectionDown;
    
    
    [self.view addGestureRecognizer:self.leftSwipeGestureRecognizer];
    [self.view addGestureRecognizer:self.rightSwipeGestureRecognizer];
    [self.view addGestureRecognizer:self.upSwipeGestureRecognizer];
    [self.view addGestureRecognizer:self.downSwipeGestureRecognizer];
    
    
    
    
    
    //Set all tiles to empty.
    for(int i=1;i<17;i++){
        [[labels objectAtIndex:i] setText:@""];
        [self GeneratingColor];
    }
    

    
    
    
    //Initial 2 tiles declaration
    
    NSInteger Random = arc4random_uniform(16) +1 ;
    
    while(![[[labels objectAtIndex:Random] text] isEqual:@""]){
        Random = arc4random_uniform(16) +1 ;
        
    }
    
    [[labels objectAtIndex:Random] setText:@"2"];
    [self GeneratingColor];
    
    
    NSInteger Random2 = arc4random_uniform(16) +1;
    
    while(![[[labels objectAtIndex:Random2] text] isEqual:@""]){
        Random2 = arc4random_uniform(16) +1 ;
        
    }
    
    [[labels objectAtIndex:Random2] setText:@"2"];
    [self GeneratingColor];

}

-(void)GeneratingColor
{
///adding colors to tiles
    
    //Code for Game Over.
    if([[[labels objectAtIndex:1] text] isEqual:@""] || [[[labels objectAtIndex:2] text] isEqual:@""] || [[[labels objectAtIndex:3] text] isEqual:@""] || [[[labels objectAtIndex:4] text] isEqual:@""] || [[[labels objectAtIndex:5] text] isEqual:@""] || [[[labels objectAtIndex:6] text] isEqual:@""] || [[[labels objectAtIndex:7] text] isEqual:@""] || [[[labels objectAtIndex:8] text] isEqual:@""] || [[[labels objectAtIndex:9] text] isEqual:@""] ||[[[labels objectAtIndex:1] text] isEqual:@""] ||[[[labels objectAtIndex:1] text] isEqual:@""] || [[[labels objectAtIndex:10] text] isEqual:@""] || [[[labels objectAtIndex:11] text] isEqual:@""] || [[[labels objectAtIndex:12] text] isEqual:@""] || [[[labels objectAtIndex:13] text] isEqual:@""] || [[[labels objectAtIndex:14] text] isEqual:@""] || [[[labels objectAtIndex:15] text] isEqual:@""] || [[[labels objectAtIndex:16] text] isEqual:@""])
        {
               [Gameover setText:@""];
        }
    else
        {
        [scoreLabel setText:@"Game Over"];
        [Gameover setText:@"Oops, Game Over!"];
        }
      
    
    
for(int lb = 1; lb <=16;lb++)
{
    if([[[labels objectAtIndex:lb] text] isEqual:@""])
        {
        [[labels objectAtIndex:lb] setBackgroundColor:[UIColor cyanColor]];
        }
    else
        {
        if([[[labels objectAtIndex:lb] text] isEqual:@"2"])
            {
            [[labels objectAtIndex:lb] setTextColor:[UIColor colorWithRed:(24/255.f) green:(24/255.f) blue:(24/255.f) alpha:1]];
            [[labels objectAtIndex:lb] setBackgroundColor:[UIColor colorWithRed:(192/255.f) green:(255/255.f) blue:(192/255.f) alpha:1]];
            
            }
        if([[[labels objectAtIndex:lb] text] isEqual:@"4"])
            {
            [UIView transitionWithView:[labels objectAtIndex:lb] duration:0.25 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
                [[labels objectAtIndex:lb] setTextColor:[UIColor colorWithRed:(24/255.f) green:(24/255.f) blue:(24/255.f) alpha:1]];
                
                [[labels objectAtIndex:lb] setBackgroundColor:[UIColor colorWithRed:(128/255.f) green:(192/255.f) blue:(128/255.f) alpha:1]];
                
                
                [[labels objectAtIndex:lb] setShadowColor:[UIColor blueColor]];
            } completion:nil];
            }
        if([[[labels objectAtIndex:lb] text] isEqual:@"8"])
            {
            [UIView transitionWithView:[labels objectAtIndex:lb] duration:0.25 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
                [[labels objectAtIndex:lb] setTextColor:[UIColor colorWithRed:(24/255.f) green:(24/255.f) blue:(24/255.f) alpha:1]];
                
                [[labels objectAtIndex:lb] setBackgroundColor:[UIColor colorWithRed:(255/255.f) green:(255/255.f) blue:(146/255.f) alpha:1]];
                
                
                [[labels objectAtIndex:lb] setShadowColor:[UIColor blueColor]];
            } completion:nil];
            }
        if([[[labels objectAtIndex:lb] text] isEqual:@"16"])
            {
            [UIView transitionWithView:[labels objectAtIndex:lb] duration:0.25 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
                [[labels objectAtIndex:lb] setTextColor:[UIColor colorWithRed:(24/255.f) green:(24/255.f) blue:(24/255.f) alpha:1]];
                
                [[labels objectAtIndex:lb] setBackgroundColor:[UIColor colorWithRed:(255/255.f) green:(255/255.f) blue:(64/255.f) alpha:1]];
                
                
                [[labels objectAtIndex:lb] setShadowColor:[UIColor blueColor]];
            } completion:nil];
            }
        if([[[labels objectAtIndex:lb] text] isEqual:@"32"])
            {
            [UIView transitionWithView:[labels objectAtIndex:lb] duration:0.25 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
                [[labels objectAtIndex:lb] setTextColor:[UIColor colorWithRed:(224/255.f) green:(224/255.f) blue:(224/255.f) alpha:1]];
                
                [[labels objectAtIndex:lb] setBackgroundColor:[UIColor colorWithRed:(255/255.f) green:(255/255.f) blue:(136/255.f) alpha:1]];
                
                
                [[labels objectAtIndex:lb] setShadowColor:[UIColor blueColor]];
            } completion:nil];
            }
        if([[[labels objectAtIndex:lb] text] isEqual:@"64"])
            {
            [UIView transitionWithView:[labels objectAtIndex:lb] duration:0.25 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
                [[labels objectAtIndex:lb] setTextColor:[UIColor blackColor]];
                
                [[labels objectAtIndex:lb] setBackgroundColor:[UIColor colorWithRed:(224/255.f) green:(224/255.f) blue:(224/255.f) alpha:1]];
                
                
                [[labels objectAtIndex:lb] setShadowColor:[UIColor colorWithRed:(255/255.f) green:(192/255.f) blue:(64/255.f) alpha:1]];
                
            } completion:nil];
            }
        if([[[labels objectAtIndex:lb] text] isEqual:@"128"])
            {
            [UIView transitionWithView:[labels objectAtIndex:lb] duration:0.25 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
                [[labels objectAtIndex:lb] setTextColor:[UIColor colorWithRed:(224/255.f) green:(224/255.f) blue:(224/255.f) alpha:1]];
                
                [[labels objectAtIndex:lb] setBackgroundColor:[UIColor colorWithRed:(255/255.f) green:(192/255.f) blue:(128/255.f) alpha:1]];
                
                
                [[labels objectAtIndex:lb] setShadowColor:[UIColor blueColor]];
            } completion:nil];
            }
        
        if([[[labels objectAtIndex:lb] text] isEqual:@"256"])
            {
            [UIView transitionWithView:[labels objectAtIndex:lb] duration:0.25 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
                [[labels objectAtIndex:lb] setTextColor:[UIColor colorWithRed:(224/255.f) green:(224/255.f) blue:(224/255.f) alpha:1]];
                
                [[labels objectAtIndex:lb] setBackgroundColor:[UIColor colorWithRed:(192/255.f) green:(128/255.f) blue:(128/255.f) alpha:1]];
                
                
                [[labels objectAtIndex:lb] setShadowColor:[UIColor blueColor]];
            } completion:nil];
            }
        if([[[labels objectAtIndex:lb] text] isEqual:@"512"])
            {
            [UIView transitionWithView:[labels objectAtIndex:lb] duration:0.25 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
                [[labels objectAtIndex:lb] setTextColor:[UIColor colorWithRed:(224/255.f) green:(224/255.f) blue:(224/255.f) alpha:1]];
                
                [[labels objectAtIndex:lb] setBackgroundColor:[UIColor colorWithRed:(192/255.f) green:(64/255.f) blue:(64/255.f) alpha:1]];
                
                
                [[labels objectAtIndex:lb] setShadowColor:[UIColor blueColor]];
            } completion:nil];
            }
        
        if([[[labels objectAtIndex:lb] text] isEqual:@"1024"])
            {
            [UIView transitionWithView:[labels objectAtIndex:lb] duration:0.25 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
                [[labels objectAtIndex:lb] setTextColor:[UIColor colorWithRed:(255/255.f) green:(000/255.f) blue:(000/255.f) alpha:1]];
                
                [[labels objectAtIndex:lb] setBackgroundColor:[UIColor colorWithRed:(000/255.f) green:(000/255.f) blue:(255/255.f) alpha:1]];
                
                
                [[labels objectAtIndex:lb] setShadowColor:[UIColor blueColor]];
            } completion:nil];
            }
        
        if([[[labels objectAtIndex:lb] text] isEqual:@"2048"])
            {
            [UIView transitionWithView:[labels objectAtIndex:lb] duration:0.25 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
                [[labels objectAtIndex:lb] setTextColor:[UIColor blackColor]];
                
                [[labels objectAtIndex:lb] setBackgroundColor:[UIColor colorWithRed:(255/255.f) green:(255/255.f) blue:(255/255.f) alpha:1]];
                
                
                [[labels objectAtIndex:lb] setShadowColor:[UIColor blueColor]];
            } completion:nil];
            }
        
        
        }

        
}

}


//Swiping in Game

- (void)handleSwipes:(UISwipeGestureRecognizer *)sender
{
    
    NSInteger Random = arc4random_uniform(16) +1 ;
    
    //left swipe
    if (sender.direction == UISwipeGestureRecognizerDirectionLeft)
        {
        
        //making tiles go left
        int tile = 1;
        while (tile <= 13) {
            if ([[[labels objectAtIndex:tile] text] isEqual:@""]) {
                [[labels objectAtIndex:tile] setText:[[labels objectAtIndex:(tile+1)] text]];
                [[labels objectAtIndex:(tile+1)] setText:@""];
    
                
            }
            
            if ([[[labels objectAtIndex:tile+1] text] isEqual:@""]){
                if([[[labels objectAtIndex:tile] text] isEqual:@""]){
                    [[labels objectAtIndex:tile] setText:[[labels objectAtIndex:(tile+2)] text]];
                    [[labels objectAtIndex:(tile+2)] setText:@""];
                    
                }
                else{
                    [[labels objectAtIndex:tile+1] setText:[[labels objectAtIndex:(tile+2)] text]];
                    [[labels objectAtIndex:(tile+2)] setText:@""];
                    
                }
            }
            
            if ([[[labels objectAtIndex:tile+2] text] isEqual:@""]){
                if([[[labels objectAtIndex:tile+1] text] isEqual:@""]){
                    [[labels objectAtIndex:tile+1] setText:[[labels objectAtIndex:(tile+3)] text]];
                    [[labels objectAtIndex:(tile+3)] setText:@""];
                    
                }
                
                else if([[[labels objectAtIndex:tile] text] isEqual:@""]){
                    [[labels objectAtIndex:tile] setText:[[labels objectAtIndex:(tile+3)] text]];
                    [[labels objectAtIndex:(tile+3)] setText:@""];
                    
                }
                
                else{
                    [[labels objectAtIndex:tile+2] setText:[[labels objectAtIndex:(tile+3)] text]];
                    [[labels objectAtIndex:(tile+3)] setText:@""];
                    
                }
                
                
            }
            
            tile+=4;
            
            
        }
        
        //Adding logic
        int k = 1;
        while(k<=13)
            {
            if ([[[labels objectAtIndex:k] text] isEqualToString:[[labels objectAtIndex:(k+1)] text]]) {
                
                int m = [[[labels objectAtIndex:k] text] intValue];
                int n = [[[labels objectAtIndex:(k+1)] text] intValue];
               int res = m + n;
                
                if(res > 0)
                    {
                    [[labels objectAtIndex:k] setText :[NSString stringWithFormat:@"%d",res] ];
                             int p = [scoreLabel.text intValue];
                          int scr = p + res;
                         [scoreLabel setText:[NSString stringWithFormat:@"%d",scr]];
                    [animateLabel setText:[NSString stringWithFormat:@"+%d",res]];
                  
                    //emptying the 2nd tile
                    [[labels objectAtIndex:(k+1)] setText:@""];
                    // 	  [[labels objectAtIndex:(k+1)] setBackgroundColor:[UIColor colorWithRed:(211/255.f) green:(211/255.f) blue:(211/255.f) alpha:1]];
                    }
            }
            
            //adding 2nd and 3rd column
            if ([[[labels objectAtIndex:(k+1)] text] isEqualToString:[[labels objectAtIndex:(k+2)] text]]) {
                
                int m = [[[labels objectAtIndex:(k+1)] text] intValue];
                int n = [[[labels objectAtIndex:(k+2)] text] intValue];
                int res = m + n;
                
                if(res > 0)
                    {
                    [[labels objectAtIndex:(k+1)] setText :[NSString stringWithFormat:@"%d",res] ];
                       int p = [scoreLabel.text intValue];
                     int scr = p + res;
                     [scoreLabel setText:[NSString stringWithFormat:@"%d",scr]];
                  [animateLabel setText:[NSString stringWithFormat:@"+%d",res]];
                    //emptying the 2nd tile
                    [[labels objectAtIndex:(k+2)] setText:@""];
                    //    [[labels objectAtIndex:(k+2)] setBackgroundColor:[UIColor colorWithRed:(211/255.f) green:(211/255.f) blue:(211/255.f) alpha:1]];
                    }
            }
            
            //adding 3rd and 4th column
            if ([[[labels objectAtIndex:(k+2)] text] isEqualToString:[[labels objectAtIndex:(k+3)] text]]) {
                
                int m = [[[labels objectAtIndex:(k+2)] text] intValue];
                int n = [[[labels objectAtIndex:(k+3)] text] intValue];
                int res = m + n;
                
                if(res > 0)
                    {
                    [[labels objectAtIndex:(k+2)] setText :[NSString stringWithFormat:@"%d",res] ];
                        int p = [scoreLabel.text intValue];
                     int scr = p + res;
                     [scoreLabel setText:[NSString stringWithFormat:@"%d",scr]];
                   [animateLabel setText:[NSString stringWithFormat:@"+%d",res]];
                    //emptying the 2nd tile
                    [[labels objectAtIndex:(k+3)] setText:@""];
                    //      [[labels objectAtIndex:(k+3)] setBackgroundColor:[UIColor colorWithRed:(211/255.f) green:(211/255.f) blue:(211/255.f) alpha:1]];
                    }
            }
            
            k = k + 4;
            }
        
        tile=1;
        while (tile <= 13) {
            if ([[[labels objectAtIndex:tile] text] isEqual:@""]) {
                [[labels objectAtIndex:tile] setText:[[labels objectAtIndex:(tile+1)] text]];
                [[labels objectAtIndex:(tile+1)] setText:@""];
              
                
            }
            
            if ([[[labels objectAtIndex:tile+1] text] isEqual:@""]){
                if([[[labels objectAtIndex:tile] text] isEqual:@""]){
                    [[labels objectAtIndex:tile] setText:[[labels objectAtIndex:(tile+2)] text]];
                    [[labels objectAtIndex:(tile+2)] setText:@""];
                    
                }
                else{
                    [[labels objectAtIndex:tile+1] setText:[[labels objectAtIndex:(tile+2)] text]];
                    [[labels objectAtIndex:(tile+2)] setText:@""];
                    
                }
            }
            
            if ([[[labels objectAtIndex:tile+2] text] isEqual:@""]){
                if([[[labels objectAtIndex:tile+1] text] isEqual:@""]){
                    [[labels objectAtIndex:tile+1] setText:[[labels objectAtIndex:(tile+3)] text]];
                    [[labels objectAtIndex:(tile+3)] setText:@""];
                    
                }
                
                else if([[[labels objectAtIndex:tile] text] isEqual:@""]){
                    [[labels objectAtIndex:tile] setText:[[labels objectAtIndex:(tile+3)] text]];
                    [[labels objectAtIndex:(tile+3)] setText:@""];
                    
                }
                
                else{
                    [[labels objectAtIndex:tile+2] setText:[[labels objectAtIndex:(tile+3)] text]];
                    [[labels objectAtIndex:(tile+3)] setText:@""];
                    
                }
                
                
            }
            
            tile+=4;
            
            
        }
     while(![[[labels objectAtIndex:Random] text] isEqual:@""]){
            Random = arc4random_uniform(16) +1 ;
            
        }
        
        [[labels objectAtIndex:Random] setText:@"2"];
        [self GeneratingColor];
        
        }
    
    
    //Right swipe
    if (sender.direction == UISwipeGestureRecognizerDirectionRight)
        {
        
        //making tiles go right
      
        int tile = 1;
        while (tile <= 13) {
            if ([[[labels objectAtIndex:tile+3] text] isEqual:@""]) {
                [[labels objectAtIndex:tile+3] setText:[[labels objectAtIndex:(tile+2)] text]];
                [[labels objectAtIndex:(tile+2)] setText:@""];
            
                
            }
            
            if ([[[labels objectAtIndex:tile+2] text] isEqual:@""]){
                if([[[labels objectAtIndex:tile+3] text] isEqual:@""]){
                    [[labels objectAtIndex:tile+3] setText:[[labels objectAtIndex:(tile+1)] text]];
                    [[labels objectAtIndex:(tile+1)] setText:@""];
                    
                }
                else{
                    [[labels objectAtIndex:tile+2] setText:[[labels objectAtIndex:(tile+1)] text]];
                    [[labels objectAtIndex:(tile+1)] setText:@""];
                    
                }
            }
            
            if ([[[labels objectAtIndex:tile+1] text] isEqual:@""]){
                if([[[labels objectAtIndex:tile+3] text] isEqual:@""]){
                    [[labels objectAtIndex:tile+3] setText:[[labels objectAtIndex:(tile)] text]];
                    [[labels objectAtIndex:(tile)] setText:@""];
                    
                }
                
                else if([[[labels objectAtIndex:tile+2] text] isEqual:@""]){
                    [[labels objectAtIndex:tile+2] setText:[[labels objectAtIndex:(tile)] text]];
                    [[labels objectAtIndex:(tile)] setText:@""];
                    
                }
                
                else{
                    [[labels objectAtIndex:tile+1] setText:[[labels objectAtIndex:(tile)] text]];
                    [[labels objectAtIndex:(tile)] setText:@""];
                    
                }
                
                
            }
            
            tile+=4;
            
            
        }
        //Adding logic
        int k = 1;
        while(k<=13)
            {
            if ([[[labels objectAtIndex:k+3] text] isEqualToString:[[labels objectAtIndex:(k+2)] text]]) {
                
                int m = [[[labels objectAtIndex:k+3] text] intValue];
                int n = [[[labels objectAtIndex:(k+2)] text] intValue];
                int res = m + n;
                
                if(res > 0)
                    {
                    [[labels objectAtIndex:k+3] setText :[NSString stringWithFormat:@"%d",res] ];
                             int p = [scoreLabel.text intValue];
                           int scr = p + res;
                         [scoreLabel setText:[NSString stringWithFormat:@"%d",scr]];
                    [animateLabel setText:[NSString stringWithFormat:@"+%d",res]];
                    //emptying the 2nd tile
                    [[labels objectAtIndex:(k+2)] setText:@""];
                    // 	  [[labels objectAtIndex:(k+1)] setBackgroundColor:[UIColor colorWithRed:(211/255.f) green:(211/255.f) blue:(211/255.f) alpha:1]];
                    }
            }
            
            //adding 2nd and 3rd column
            if ([[[labels objectAtIndex:(k+2)] text] isEqualToString:[[labels objectAtIndex:(k+1)] text]]) {
                
                int m = [[[labels objectAtIndex:(k+2)] text] intValue];
                int n = [[[labels objectAtIndex:(k+1)] text] intValue];
                int res = m + n;
                
                if(res > 0)
                    {
                    [[labels objectAtIndex:(k+2)] setText :[NSString stringWithFormat:@"%d",res] ];
                       int p = [scoreLabel.text intValue];
                     int scr = p + res;
                     [scoreLabel setText:[NSString stringWithFormat:@"%d",scr]];
                    [animateLabel setText:[NSString stringWithFormat:@"+%d",res]];
                    //emptying the 2nd tile
                    [[labels objectAtIndex:(k+1)] setText:@""];
                    //    [[labels objectAtIndex:(k+2)] setBackgroundColor:[UIColor colorWithRed:(211/255.f) green:(211/255.f) blue:(211/255.f) alpha:1]];
                    }
            }
            
            //adding 3rd and 4th column
            if ([[[labels objectAtIndex:(k+1)] text] isEqualToString:[[labels objectAtIndex:(k)] text]]) {
                
                int m = [[[labels objectAtIndex:(k+1)] text] intValue];
                int n = [[[labels objectAtIndex:(k)] text] intValue];
                int res = m + n;
                
                if(res > 0)
                    {
                    [[labels objectAtIndex:(k+1)] setText :[NSString stringWithFormat:@"%d",res] ];
                        int p = [scoreLabel.text intValue];
                     int scr = p + res;
                     [scoreLabel setText:[NSString stringWithFormat:@"%d",scr]];
                    [animateLabel setText:[NSString stringWithFormat:@"+%d",res]];
                    //emptying the 2nd tile
                    [[labels objectAtIndex:(k)] setText:@""];
                    //      [[labels objectAtIndex:(k+3)] setBackgroundColor:[UIColor colorWithRed:(211/255.f) green:(211/255.f) blue:(211/255.f) alpha:1]];
                    }
            }
            
            k = k + 4;
            }
        
        tile=1;
        while (tile <= 13) {
            if ([[[labels objectAtIndex:tile+3] text] isEqual:@""]) {
                [[labels objectAtIndex:tile+3] setText:[[labels objectAtIndex:(tile+2)] text]];
                [[labels objectAtIndex:(tile+2)] setText:@""];
                
                
            }
            
            if ([[[labels objectAtIndex:tile+2] text] isEqual:@""]){
                if([[[labels objectAtIndex:tile+3] text] isEqual:@""]){
                    [[labels objectAtIndex:tile+3] setText:[[labels objectAtIndex:(tile+1)] text]];
                    [[labels objectAtIndex:(tile+1)] setText:@""];
                    
                }
                else{
                    [[labels objectAtIndex:tile+2] setText:[[labels objectAtIndex:(tile+1)] text]];
                    [[labels objectAtIndex:(tile+1)] setText:@""];
                    
                }
            }
            
            if ([[[labels objectAtIndex:tile+1] text] isEqual:@""]){
                if([[[labels objectAtIndex:tile+3] text] isEqual:@""]){
                    [[labels objectAtIndex:tile+3] setText:[[labels objectAtIndex:(tile)] text]];
                    [[labels objectAtIndex:(tile)] setText:@""];
                    
                }
                
                else if([[[labels objectAtIndex:tile+2] text] isEqual:@""]){
                    [[labels objectAtIndex:tile+2] setText:[[labels objectAtIndex:(tile)] text]];
                    [[labels objectAtIndex:(tile)] setText:@""];
                    
                }
                
                else{
                    [[labels objectAtIndex:tile+1] setText:[[labels objectAtIndex:(tile)] text]];
                    [[labels objectAtIndex:(tile)] setText:@""];
                    
                }
                
                
            }
            
            tile+=4;
            
            
        }
        
        while(![[[labels objectAtIndex:Random] text] isEqual:@""]){
            Random = arc4random_uniform(16) +1 ;
            
        }
        
        [[labels objectAtIndex:Random] setText:@"2"];
        [self GeneratingColor];
        }
    
    
    //Upward swipe
    if (sender.direction == UISwipeGestureRecognizerDirectionUp)
        {
        
        //making tiles go Up
        
        for(int tile=1;tile<=4;tile++){
            if ([[[labels objectAtIndex:tile] text] isEqual:@""]) {
                [[labels objectAtIndex:tile] setText:[[labels objectAtIndex:(tile+4)] text]];
                [[labels objectAtIndex:(tile+4)] setText:@""];
                
                
            }
            
            if ([[[labels objectAtIndex:tile+4] text] isEqual:@""]){
                if([[[labels objectAtIndex:tile] text] isEqual:@""]){
                    [[labels objectAtIndex:tile] setText:[[labels objectAtIndex:(tile+8)] text]];
                    [[labels objectAtIndex:(tile+8)] setText:@""];
                    
                }
                else{
                    [[labels objectAtIndex:tile+4] setText:[[labels objectAtIndex:(tile+8)] text]];
                    [[labels objectAtIndex:(tile+8)] setText:@""];
                    
                }
            }
            
            if ([[[labels objectAtIndex:tile+8] text] isEqual:@""]){
                if([[[labels objectAtIndex:tile] text] isEqual:@""]){
                    [[labels objectAtIndex:tile] setText:[[labels objectAtIndex:(tile+12)] text]];
                    [[labels objectAtIndex:(tile+12)] setText:@""];
                    
                }
                
                else if([[[labels objectAtIndex:tile+4] text] isEqual:@""]){
                    [[labels objectAtIndex:tile+4] setText:[[labels objectAtIndex:(tile+12)] text]];
                    [[labels objectAtIndex:(tile+12)] setText:@""];
                    
                }
                
                else{
                    [[labels objectAtIndex:tile+8] setText:[[labels objectAtIndex:(tile+12)] text]];
                    [[labels objectAtIndex:(tile+12)] setText:@""];
                    
                }
                
                
            }
            
            
            
        }
        
    
    for(int k=1;k<=4;k++)
        {
        if ([[[labels objectAtIndex:k] text] isEqualToString:[[labels objectAtIndex:(k+4)] text]]) {
            
            int m = [[[labels objectAtIndex:k] text] intValue];
            int n = [[[labels objectAtIndex:(k+4)] text] intValue];
            int res = m + n;
            
            if(res > 0)
                {
                [[labels objectAtIndex:k] setText :[NSString stringWithFormat:@"%d",res] ];
                         int p = [scoreLabel.text intValue];
                       int scr = p + res;
                 [scoreLabel setText:[NSString stringWithFormat:@"%d",scr]];
                [animateLabel setText:[NSString stringWithFormat:@"+%d",res]];
                //emptying the 2nd tile
                [[labels objectAtIndex:(k+4)] setText:@""];
                // 	  [[labels objectAtIndex:(k+1)] setBackgroundColor:[UIColor colorWithRed:(211/255.f) green:(211/255.f) blue:(211/255.f) alpha:1]];
                }
        }
        
        //adding 2nd and 3rd row
        if ([[[labels objectAtIndex:(k+4)] text] isEqualToString:[[labels objectAtIndex:(k+8)] text]]) {
            
            int m = [[[labels objectAtIndex:(k+4)] text] intValue];
            int n = [[[labels objectAtIndex:(k+8)] text] intValue];
            int res = m + n;
            
            if(res > 0)
                {
                [[labels objectAtIndex:(k+4)] setText :[NSString stringWithFormat:@"%d",res] ];
                  int p = [scoreLabel.text intValue];
                 int scr = p + res;
                 [scoreLabel setText:[NSString stringWithFormat:@"%d",scr]];
                [animateLabel setText:[NSString stringWithFormat:@"+%d",res]];
                //emptying the 2nd tile
                [[labels objectAtIndex:(k+8)] setText:@""];
                //    [[labels objectAtIndex:(k+2)] setBackgroundColor:[UIColor colorWithRed:(211/255.f) green:(211/255.f) blue:(211/255.f) alpha:1]];
                }
        }
        
        //adding 3rd and 4th row
        if ([[[labels objectAtIndex:(k+8)] text] isEqualToString:[[labels objectAtIndex:(k+12)] text]]) {
            
            int m = [[[labels objectAtIndex:(k+8)] text] intValue];
            int n = [[[labels objectAtIndex:(k+12)] text] intValue];
            int res = m + n;
            
            if(res > 0)
                {
                [[labels objectAtIndex:(k+8)] setText :[NSString stringWithFormat:@"%d",res] ];
                   int p = [scoreLabel.text intValue];
                 int scr = p + res;
                 [scoreLabel setText:[NSString stringWithFormat:@"%d",scr]];
                [animateLabel setText:[NSString stringWithFormat:@"+%d",res]];
                //emptying the 2nd tile
                [[labels objectAtIndex:(k+12)] setText:@""];
                //      [[labels objectAtIndex:(k+3)] setBackgroundColor:[UIColor colorWithRed:(211/255.f) green:(211/255.f) blue:(211/255.f) alpha:1]];
                }
        }
        }
        
        
        for(int tile=1;tile<=4;tile++){
            if ([[[labels objectAtIndex:tile] text] isEqual:@""]) {
                [[labels objectAtIndex:tile] setText:[[labels objectAtIndex:(tile+4)] text]];
                [[labels objectAtIndex:(tile+4)] setText:@""];
                
                
            }
            
            if ([[[labels objectAtIndex:tile+4] text] isEqual:@""]){
                if([[[labels objectAtIndex:tile] text] isEqual:@""]){
                    [[labels objectAtIndex:tile] setText:[[labels objectAtIndex:(tile+8)] text]];
                    [[labels objectAtIndex:(tile+8)] setText:@""];
                    
                }
                else{
                    [[labels objectAtIndex:tile+4] setText:[[labels objectAtIndex:(tile+8)] text]];
                    [[labels objectAtIndex:(tile+8)] setText:@""];
                    
                }
            }
            
            if ([[[labels objectAtIndex:tile+8] text] isEqual:@""]){
                if([[[labels objectAtIndex:tile] text] isEqual:@""]){
                    [[labels objectAtIndex:tile] setText:[[labels objectAtIndex:(tile+12)] text]];
                    [[labels objectAtIndex:(tile+12)] setText:@""];
                    
                }
                
                else if([[[labels objectAtIndex:tile+4] text] isEqual:@""]){
                    [[labels objectAtIndex:tile+4] setText:[[labels objectAtIndex:(tile+12)] text]];
                    [[labels objectAtIndex:(tile+12)] setText:@""];
                    
                }
                
                else{
                    [[labels objectAtIndex:tile+8] setText:[[labels objectAtIndex:(tile+12)] text]];
                    [[labels objectAtIndex:(tile+12)] setText:@""];
                    
                }
                
                
            }
        }
        while(![[[labels objectAtIndex:Random] text] isEqual:@""]){
            Random = arc4random_uniform(16) +1 ;
            
        }
        
        [[labels objectAtIndex:Random] setText:@"2"];
        [self GeneratingColor];
        
    
    
}
   
    //Downward swipe
    if (sender.direction == UISwipeGestureRecognizerDirectionDown)
        {
        
        //making tiles go Down
        
        for(int tile=1;tile<=4;tile++){
            if ([[[labels objectAtIndex:tile+12] text] isEqual:@""]) {
                [[labels objectAtIndex:tile+12] setText:[[labels objectAtIndex:(tile+8)] text]];
                [[labels objectAtIndex:(tile+8)] setText:@""];
                
                
            }
            
            if ([[[labels objectAtIndex:tile+8] text] isEqual:@""]){
                if([[[labels objectAtIndex:tile+12] text] isEqual:@""]){
                    [[labels objectAtIndex:tile+12] setText:[[labels objectAtIndex:(tile+4)] text]];
                    [[labels objectAtIndex:(tile+4)] setText:@""];
                    
                }
                else{
                    [[labels objectAtIndex:tile+8] setText:[[labels objectAtIndex:(tile+4)] text]];
                    [[labels objectAtIndex:(tile+4)] setText:@""];
                    
                }
            }
            
            if ([[[labels objectAtIndex:tile+4] text] isEqual:@""]){
                if([[[labels objectAtIndex:tile+12] text] isEqual:@""]){
                    [[labels objectAtIndex:tile+12] setText:[[labels objectAtIndex:(tile)] text]];
                    [[labels objectAtIndex:(tile)] setText:@""];
                    
                }
                
                else if([[[labels objectAtIndex:tile+8] text] isEqual:@""]){
                    [[labels objectAtIndex:tile+8] setText:[[labels objectAtIndex:(tile)] text]];
                    [[labels objectAtIndex:(tile)] setText:@""];
                    
                }
                
                else{
                    [[labels objectAtIndex:tile+4] setText:[[labels objectAtIndex:(tile)] text]];
                    [[labels objectAtIndex:(tile)] setText:@""];
                    
                }
                
                
            }
            
            
            
        }
        
        
        for(int k=1;k<=4;k++)
            {
            if ([[[labels objectAtIndex:k+12] text] isEqualToString:[[labels objectAtIndex:(k+8)] text]]) {
                
                int m = [[[labels objectAtIndex:k+12] text] intValue];
                int n = [[[labels objectAtIndex:(k+8)] text] intValue];
                int res = m + n;
                
                if(res > 0)
                    {
                    [[labels objectAtIndex:k+12] setText :[NSString stringWithFormat:@"%d",res] ];
                             int p = [scoreLabel.text intValue];
                           int scr = p + res;
                         [scoreLabel setText:[NSString stringWithFormat:@"%d",scr]];
                    [animateLabel setText:[NSString stringWithFormat:@"+%d",res]];
                    //emptying the 2nd tile
                    [[labels objectAtIndex:(k+8)] setText:@""];
                    // 	  [[labels objectAtIndex:(k+1)] setBackgroundColor:[UIColor colorWithRed:(211/255.f) green:(211/255.f) blue:(211/255.f) alpha:1]];
                    }
            }
            
            //adding 2nd and 3rd row
            if ([[[labels objectAtIndex:(k+8)] text] isEqualToString:[[labels objectAtIndex:(k+4)] text]]) {
                
                int m = [[[labels objectAtIndex:(k+8)] text] intValue];
                int n = [[[labels objectAtIndex:(k+4)] text] intValue];
                int res = m + n;
                
                if(res > 0)
                    {
                    [[labels objectAtIndex:(k+8)] setText :[NSString stringWithFormat:@"%d",res] ];
                       int p = [scoreLabel.text intValue];
                     int scr = p + res;
                     [scoreLabel setText:[NSString stringWithFormat:@"%d",scr]];
                    [animateLabel setText:[NSString stringWithFormat:@"+%d",res]];
                    //emptying the 2nd tile
                    [[labels objectAtIndex:(k+4)] setText:@""];
                    //    [[labels objectAtIndex:(k+2)] setBackgroundColor:[UIColor colorWithRed:(211/255.f) green:(211/255.f) blue:(211/255.f) alpha:1]];
                    }
            }
            
            //adding 3rd and 4th row
            if ([[[labels objectAtIndex:(k+4)] text] isEqualToString:[[labels objectAtIndex:(k)] text]]) {
                
                int m = [[[labels objectAtIndex:(k+4)] text] intValue];
                int n = [[[labels objectAtIndex:(k)] text] intValue];
                int res = m + n;
                
                if(res > 0)
                    {
                    [[labels objectAtIndex:(k+4)] setText :[NSString stringWithFormat:@"%d",res] ];
                       int p = [scoreLabel.text intValue];
                     int scr = p + res;
                     [scoreLabel setText:[NSString stringWithFormat:@"%d",scr]];
                    [animateLabel setText:[NSString stringWithFormat:@"+%d",res]];
                    //emptying the 2nd tile
                    [[labels objectAtIndex:(k)] setText:@""];
                    //      [[labels objectAtIndex:(k+3)] setBackgroundColor:[UIColor colorWithRed:(211/255.f) green:(211/255.f) blue:(211/255.f) alpha:1]];
                    }
            }
            }
        
        for(int tile=1;tile<=4;tile++){
            if ([[[labels objectAtIndex:tile+12] text] isEqual:@""]) {
                [[labels objectAtIndex:tile+12] setText:[[labels objectAtIndex:(tile+8)] text]];
                [[labels objectAtIndex:(tile+8)] setText:@""];
                
                
            }
            
            if ([[[labels objectAtIndex:tile+8] text] isEqual:@""]){
                if([[[labels objectAtIndex:tile+12] text] isEqual:@""]){
                    [[labels objectAtIndex:tile+12] setText:[[labels objectAtIndex:(tile+4)] text]];
                    [[labels objectAtIndex:(tile+4)] setText:@""];
                    
                }
                else{
                    [[labels objectAtIndex:tile+8] setText:[[labels objectAtIndex:(tile+4)] text]];
                    [[labels objectAtIndex:(tile+4)] setText:@""];
                    
                }
            }
            
            if ([[[labels objectAtIndex:tile+4] text] isEqual:@""]){
                if([[[labels objectAtIndex:tile+12] text] isEqual:@""]){
                    [[labels objectAtIndex:tile+12] setText:[[labels objectAtIndex:(tile)] text]];
                    [[labels objectAtIndex:(tile)] setText:@""];
                    
                }
                
                else if([[[labels objectAtIndex:tile+8] text] isEqual:@""]){
                    [[labels objectAtIndex:tile+8] setText:[[labels objectAtIndex:(tile)] text]];
                    [[labels objectAtIndex:(tile)] setText:@""];
                    
                }
                
                else{
                    [[labels objectAtIndex:tile+4] setText:[[labels objectAtIndex:(tile)] text]];
                    [[labels objectAtIndex:(tile)] setText:@""];
                    
                }
                
                
            }
            
            
            
        }
        
        while(![[[labels objectAtIndex:Random] text] isEqual:@""]){
            Random = arc4random_uniform(16) +1 ;
            
        }
        
        [[labels objectAtIndex:Random] setText:@"2"];
        [self GeneratingColor];
        
        
        
        }

    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
