//
//  AppDelegate.h
//  2048-Game
//
//  Created by Chiragkishore Khubchandani on 2/1/17.
//  Copyright © 2017 Chirag Khubchandani. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

