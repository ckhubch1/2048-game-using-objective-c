//
//  ViewController.m
//  2048-Game
//
//  Created by Chiragkishore Khubchandani on 2/1/17.
//  Copyright © 2017 Chirag Khubchandani. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

@synthesize label1,label01,label02,label03,label05;
NSTimer *timer;
BOOL blinkStatus = NO;

- (void)viewDidLoad {
    [label01 setText: @""];
    [label02 setText: @""];
    [label03 setText: @""];
    [label05 setText: @""];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [NSTimer
                      scheduledTimerWithTimeInterval:(NSTimeInterval)(0.4)
                      target:self
                      selector:@selector(blink)
                      userInfo:nil
                      repeats:TRUE];
    
}

-(void)blink{
    if(blinkStatus == NO){
        [label1 setText:@""];
        blinkStatus = YES;
    }else {
        [label1 setText:@"2048"];
        blinkStatus = NO;
    }
}

- (IBAction)instructions:(id)sender {
    
    [label01 setText:@"Click on Start Game to play."];
    [label02 setText: @"Swipe tiles in four directions to proceed."];
    [label03 setText: @"Make as many points as you can."];
    [label05 setText: @"Have fun!"];
     
}

- (IBAction)about:(id)sender {
    [label01 setText:@"Game has a Restart Button."];
    [label02 setText: @"The tiles are generated at Random."];
    [label03 setText: @"Scoring is made simple."];
    [label05 setText: @"By Chirag Khubchandani"];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
